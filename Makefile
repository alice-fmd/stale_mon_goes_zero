#
#
#
PACKAGE		= mon_goes_zero
VERSION		= 0.2
DISTNAME	= $(PACKAGE)-$(VERSION)

CPP		= g++ -E
CPPFLAGS	= 
CXX		= g++ -c
CXXFLAGS	= 
LD		= g++ 
LDFLAGS		= -pthread
ROOT		= root

%.o:%.cxx
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%:%.o
	$(LD) $(LDFLAGS) $(LIBS) $^ -o $@ 

%_C.so:%.C
	rm -f $@
	@echo "{ gROOT->LoadMacro(\"$<++g\"); }" >> compile.C
	$(ROOT) -b -q -l compile.C
	rm -f compile.C

all:	readStatus simpleSlowMon ReadMonLog_C.so permissions

permissions:
	chmod 755 readStatus.sh simpleSlowMon.sh 

readStatus:readStatus.o
	$(LD) $(LDFLAGS) $(LIBS) $^ -o $@ 

simpleSlowMon:simpleSlowMon.o
	$(LD) $(LDFLAGS) $(LIBS) $^ -o $@ 

clean:
	rm -rf readStatus simpleSlowMon
	rm -rf *_C.d *_C.so *.o *~ 
	-mv *.log *.bad *.log.root *.log.bad logs/

realclean: clean
	rm -rf logs *.tar.gz 

dist:
	mkdir -p $(DISTNAME)
	cp readStatus.cxx 	\
	   readStatus.sh 	\
	   simpleSlowMon.cxx 	\
	   simpleSlowMon.sh 	\
	   ReadMonLog.C 	\
	   Makefile		\
	   README		\
		$(DISTNAME)/
	tar -czvf $(DISTNAME).tar.gz $(DISTNAME) 
	rm -rf $(DISTNAME)

readStatus:		LDFLAGS:=$(shell rcuxx-config --ldflags)
readStatus:		LIBS:=$(shell rcuxx-config --libs)
readStatus.o:		CPPFLAGS:=$(shell rcuxx-config --cppflags)

simpleSlowMon:		LDFLAGS:=$(shell dim-config --ldflags) -pthread
simpleSlowMon:		LIBS:=$(shell dim-config --libs) 
simpleSlowMon.o:	CPPFLAGS:=$(shell dim-config --cppflags)


