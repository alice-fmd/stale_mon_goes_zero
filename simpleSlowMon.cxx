/*
 * To compile, do 
 *
 *  g++ `dim-config --ldflags --libs` main.cxx -o simpleSlowMon -pthread 
 */
#include <dim/dic.hxx>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <cmath>

//____________________________________________________________________
class Service : public DimInfoHandler
{
public:
  Service(int id, const std::string& what)
    : fNoLink(true), 
      fIsChanged(true),
      fName("")
  {
    std::stringstream s;
    s << std::setfill('0') << std::setw(2) << id << "_" << what;
    fName = s.str();
  }
  virtual ~Service()
  {
  }
  bool IsChanged() const 
  {
    // std::cerr << fInfo->getName() << " is changed " << std::endl;
    return fIsChanged;
  }
  void Processed() 
  {
    fIsChanged = false;
  }
  static std::string MakeServiceName(const std::string& server, 
				     unsigned int id, const std::string& what)
  {
    std::stringstream s;
    s << server << "_" << std::setfill('0') << std::setw(2) << id 
      << std::setfill(' ') << "_" << what;
    return s.str();
  }
  enum {
    kHeader   = 0x1, 
    kNatural  = 0x2, 
    kValue    = 0x4, 
    kAll      = 0x7
  };
  virtual std::ostream& Print(std::ostream& o, int what) = 0;
protected:
  std::string fName;
  bool fNoLink;
  bool fIsChanged;
};

//____________________________________________________________________
class MonitorService : public Service 
{
public:
  typedef double (*ConvFunc_t)(unsigned int);
  MonitorService(const std::string& server, int id, const std::string& what,
		 ConvFunc_t conv)
    : Service(id, what),
      fConv(conv),
      fInfoValue(0),
      fInfoThr(0),
      fValue(0),
      fThreshold(0)
  {
    std::string thrName(what);
    thrName.append("_TH");
    fInfoValue = new DimStampedInfo(MakeServiceName(server,id,what).c_str(),
				    60, 0, 0, this);
    fInfoThr   = new DimStampedInfo(MakeServiceName(server,id,thrName).c_str(),
				    60, 0, 0, this);
  }
  virtual ~MonitorService()
  {
    if (fInfoValue) delete fInfoValue;
    if (fInfoThr)   delete fInfoThr;
  }
  void infoHandler()
  {
    DimInfo* info = getInfo();
    void*    data = info->getData();    
    int      size = info->getSize();

    if (size <= 0 || !data) {
      fNoLink = true;
      return;
    }
    fNoLink = false;
    
    unsigned int value;
    memcpy(&value, data, sizeof(unsigned int));

    if (info == fInfoValue) HandleValue(value);
    if (info == fInfoThr)   HandleThreshold(value);
  }
  virtual void HandleValue(unsigned int value)
  {
    fIsChanged = (abs(fValue - value) > fChangeThreshold);
    fValue     = value;
  }
  virtual void HandleThreshold(unsigned int thr) 
  {
    fIsChanged = (abs(fThreshold - thr) > fChangeThreshold);
    fThreshold = thr;
  }
  
  unsigned int Value() const
  {
    return (fNoLink ? 0xFFFFFFFF : fValue);
  }
  unsigned int Threshold() const
  {
    return (fNoLink ? 0xFFFFFFFF : fThreshold);
  }
  virtual double NaturalValue() const 
  {
    return (*fConv)(Value());
  }
  virtual double NaturalThreshold() const 
  {
    return (*fConv)(Threshold());
  }
  virtual std::ostream& Print(std::ostream& o, int what)
  {
    if (what & kHeader) {
      o << '"' << fName << "\",\"" << fName << "_TH" << '"';
      if (what & ~kHeader) 
	o << ",";
    }
    if (what & kNatural) {
      o << NaturalValue() << "," << NaturalThreshold();
      if (what & ~kNatural) 
	o << ",";
    }
    if (what & kValue) {
      o << Value() << "," << Threshold();
    }
    return o;
  }
  static unsigned int fChangeThreshold;
protected:
  DimStampedInfo* fInfoValue;
  DimStampedInfo* fInfoThr;
  ConvFunc_t      fConv;
  unsigned int fValue;
  unsigned int fThreshold;
};
unsigned int MonitorService::fChangeThreshold = 10;

//____________________________________________________________________
class Csr1 : public Service
{
public: 
  Csr1(const std::string& server, unsigned int id) 
    : Service(id, "CSR1"),
      fInfo(0)
  {
    fInfo = new DimStampedInfo(MakeServiceName(server, id, "CSR1").c_str(),
			       60, 0, 0, this);
  }
  virtual ~Csr1() 
  {
    if (fInfo) delete fInfo;
  }
  void infoHandler()
  {
    DimInfo* info = getInfo();
    void*    data = info->getData();    
    int      size = info->getSize();

    if (size <= 0 || !data) {
      fNoLink = true;
      return;
    }
    fNoLink = false;
    
    unsigned int value;
    memcpy(&value, data, sizeof(unsigned int));

    fIsChanged = fValue != value;
    fValue    = value;
  }
  unsigned int Value() const
  {
    return (fNoLink ? 0xFFFFFFFF : fValue);
  }
  std::string AsString() const
  {
    if (fNoLink) return "unknown";
    
    std::stringstream s;
    unsigned int value = fValue;
    for (int i = 13; i >= 0; i--) {
      unsigned int mask = (1 << i);
      bool isOn = (value & mask);
      if (!isOn) continue;
      switch (i) {
      case 13: s << "Int";		break;
      case 12: s << "Error";		break;
      case 11: s << "SC";		break;
      case 10: s << "ALTRO";		break;
      case  9: s << "Instr";		break;
      case  8: s << "Parity";		break;
      case  7: s << "SCLK";		break;
      case  6: s << "ALPS";		break;
      case  5: s << "PAPS";		break;
      case  4: s << "V";		break;
      case  3: s << "A";		break;
      case  2: s << "Neg_V+Ext_T";	break;
      case  1: s << "Neg_A";		break;
      case  0: s << "Int_T";		break;
      }
      value = value & ~mask;
      if (value & ~mask) s << " ";
    }
    return s.str();
  }
  
  virtual std::ostream& Print(std::ostream& o, int what)
  {
    if (what & kHeader) {
      o << '"' << fName << "_Value\",\"" << fName << '"';
      if (what & ~kHeader) 
	o << ",";
    }
    if (what & kNatural || what & kValue) {
      o << "0x" << std::setfill('0') << std::hex << std::setw(8) << Value()
	<< std::setfill(' ') << std::dec << ",\"" << AsString() << '"';
      if (what & ~kNatural) 
	o << ",";
    }
    return o;
  }
protected:
  DimStampedInfo* fInfo;
  unsigned int fValue;
};


  

//====================================================================
double funcT(unsigned int x) { return x * 0.25; }
double funcTS(unsigned int x) 
{ 
  return (100 * 0.442401 * x) / (0.3851 * (2.2 * 512 - 0.221301 * x));
}
double funcV(unsigned int x) { return 5. * x / 1023; }
double funcA(unsigned int x) { return 5.25 * x / 1023; }
double funcM(unsigned int x) 
{ 
  return ((x >= 512 ? 1 : 0) * 5. * (x - 512) - 
	  (x <  512 ? 1 : 0) * 5. * (512 - x)) / 1023;
}
 
//====================================================================
class Fec 
{
public:
  Fec(const std::string& server, unsigned int id)
    : fServer(server),  
      fId(id)
  {
    Register("CSR1", 0);
    Register("T1", funcT);
    Register("T2", funcT);
    Register("T3", funcT);
    Register("T4", funcT);
    Register("T1_SENS", funcTS);
    Register("T2_SENS", funcTS);
    Register("AL_DIG_I", funcA);
    Register("AL_DIG_U", funcV);
    Register("AL_ANA_I", funcA);
    Register("AL_ANA_U", funcV);
    Register("VA_REC_IP", funcA);
    Register("VA_REC_UP", funcV);
    Register("VA_REC_IM", funcA);
    Register("VA_REC_UM", funcM);
    Register("VA_SUP_IP", funcA);
    Register("VA_SUP_UP", funcV);
    Register("VA_SUP_IM", funcA);
    Register("VA_SUP_UM", funcM);
    Register("GTL_U",     funcV);
    Register("FLASH_I",   funcA);
  }
  void Register(const std::string& what, MonitorService::ConvFunc_t f)
  {
    Service* service = 0;
    if (what == "CSR1") 
      service = new Csr1(fServer, fId);
    else 
      service = new MonitorService(fServer, fId, what, f);
    fServices.push_back(service);
  }
  void Clear() 
  {
    for (ServiceList::iterator i=fServices.begin(); i != fServices.end(); ++i)
      delete (*i);
    fServices.clear();
  }
  std::ostream& Print(std::ostream& out, int what) const
  {
    if (what & Service::kHeader) 
      out << "\"FEC\"," << std::flush;
    else 
      out << fId << "," << std::flush;
    for (ServiceList::const_iterator i = fServices.begin(); 
	 i != fServices.end(); ++i) {
      (*i)->Print(out, what);
      out << ",";
    }
    return out;
  }
  bool IsChanged() const
  {
    for (ServiceList::const_iterator i = fServices.begin(); 
	 i != fServices.end(); ++i) 
      if ((*i)->IsChanged()) return true;
    return false;
  }
  void Processed()
  {
    for (ServiceList::iterator i = fServices.begin(); 
	 i != fServices.end(); ++i) 
      (*i)->Processed();
  }
  unsigned int Id() const { return fId; }
protected:
  std::string fServer;
  unsigned int fId;
  typedef std::vector<Service*> ServiceList;
  ServiceList fServices;
};


//____________________________________________________________________
class Rcu : public DimInfoHandler
{
public:
  Rcu(const std::string& server)
    : fServer(server), 
      fState(0),
      fStateVal(kUnknown),
      fACTFEC(0),
      fACTFECVal(0), 
      fFecs(32), 
      fIsChanged(true)
  {
    for (int i = 0; i < 32; i++) fFecs[i] = 0;
    
    std::stringstream sn;
    sn << server << "_RCU_STATE";
    fState = new DimStampedInfo(sn.str().c_str(), 10, 0, 0, this);

    std::stringstream an;
    an << server << "_AFL";
    fACTFEC = new DimStampedInfo(an.str().c_str(), 60, 0, 0, this);
  }
  void infoHandler() 
  {
    DimInfo* info = getInfo();
    if      (info == fState)  HandleState();
    else if (info == fACTFEC) HandleACTFEC();
  }
  enum {
    kNoLink = 0,
    kIdle, 
    kStandby, 
    kDownloading,
    kReady,
    kMixed,
    kError,
    kRunning,
    kUnknown
  };
  const char* StateString()
  {
    switch (fStateVal) {
    case kNoLink:      return "No_link";
    case kIdle:        return "Idle";
    case kStandby:     return "Standby";
    case kDownloading: return "Downloading";
    case kReady:       return "Ready";
    case kMixed:       return "Mixed";
    case kError:       return "Error";
    case kRunning:     return "Running";
    }
    return "Unknown";
  }
  
  void HandleState()
  {
    void* data = fState->getData();
    int   size = fState->getSize();
    if (size <= 0 || !data) 
      fStateVal = kNoLink;
    else {
      int state;
      memcpy(&state, data, sizeof(int));
      if (state != fStateVal) fIsChanged = true;
      
      switch (state) {
      case kIdle:        fStateVal = kIdle;        break;
      case kStandby:     fStateVal = kStandby;     break; 
      case kDownloading: fStateVal = kDownloading; break;
      case kReady:       fStateVal = kReady;       break;
      case kMixed:       fStateVal = kMixed;       break;
      case kError:       fStateVal = kError;       break; 
      case kRunning:     fStateVal = kRunning;     break;
      default:           fStateVal = kUnknown;     break;
      }
    }
  }
  void HandleACTFEC()
  {
    void* data = fACTFEC->getData();
    int   size = fACTFEC->getSize();
    if (size <= 0 || !data) {
      ClearFecs();
      return;
    }
    unsigned int actfec;
    memcpy(&actfec, data, sizeof(unsigned int));
    
    if (actfec != fACTFECVal) 
      UpdateFecs(actfec);
    
  }
  void ClearFecs()
  {
    for (FecList::const_iterator i = fFecs.begin(); i != fFecs.end(); ++i) {
      if (!(*i)) continue;
      delete (*i);
    }
    
  }
  void UpdateFecs(unsigned int actfec) 
  {
    fACTFECVal = actfec;
    fIsChanged = true;
    
    /* std::cerr << "ACTFEC=0x" << std::setfill('0') << std::hex 
     * << std::setw(8) << actfec << std::setfill(' ') 
     * << std::dec << std::endl;
     */
    for (int i = 0; i < 32; i++) {
      bool on  = (actfec & (1 << i));
      Fec* fec = fFecs[i];
      
      if (on && !fec) fFecs[i] = new Fec(fServer, i);
      if (!on && fec) { delete fFecs[i]; fFecs[i] = 0; }
    }
  }
  bool IsChanged() const 
  {
    if (fIsChanged) return true;
    
    for (FecList::const_iterator i = fFecs.begin(); i != fFecs.end(); ++i) {
      if (!(*i)) continue;
      if ((*i)->IsChanged()) { 
	// std::cerr << "FEC " << (*i)->Id() << " is changed" << std::endl;
	fIsChanged = true;
	return fIsChanged;
      }
    }
      
    return false;
  }
  void Processed()
  {
    fIsChanged = false;
    for (FecList::iterator i = fFecs.begin(); i != fFecs.end(); ++i) {
      if (!(*i)) continue;
      (*i)->Processed();
    }
  }
  
  void Print(std::ostream& out, int what)
  {
    if (what & Service::kHeader)
      out << "\"Time\",\"State\",\"AFL\"," << std::flush;
    else {
      time_t now = time(NULL);
      char buf[128];
      strftime(buf, 128, "%D %T", localtime(&now));
      out << '"' << buf << "\"," 
	  << '"' << StateString() << "\"," 
	  << "0x" << std::hex << std::setw(8) << std::setfill('0') 
	  << fACTFECVal << std::setfill(' ') << std::dec << "," << std::flush;
    }
    
    for (FecList::const_iterator i = fFecs.begin(); i != fFecs.end(); ++i) {
      if (!(*i)) continue;
      (*i)->Print(out, what);
    }
    out << std::endl;
    
  }
protected:
  std::string     fServer;
  DimStampedInfo* fState;
  int             fStateVal;
  DimStampedInfo* fACTFEC;
  unsigned int    fACTFECVal;
  typedef std::vector<Fec*> FecList;
  FecList fFecs;
  mutable bool fIsChanged;
  
};


    
      
    

//____________________________________________________________________
void
usage(const char* progname)
{
  std::cout << "Usage: " << progname << " [OPTIONS]\n\n"
	    << "Options:\n"
	    << "\t-h            This help\n"
	    << "\t-d HOST       DIM DNS to use\n"
	    << "\t-o FILE       Output file\n"
	    << "\t-n SERVER     Server name\n"
	    << "\t-p            Show heartbeat\n"
	    << "\t-r SECONDS    How often to check for changes\n"
	    << "\t-f            Force print every '-r SECONDS'\n"
	    << "\t-t ADC_COUNTS Change threshold\n"
	    << std::endl;
}


//____________________________________________________________________
template <typename T>
void str2val(const char* str, T& v)
{
  std::stringstream s(str);
  s >> v;
}

//____________________________________________________________________
int
main(int argc, char** argv)
{
  std::string dns("localhost");
  std::string out;
  std::string name("FMD-FEE_0_0_0");
  bool pulse = false;
  bool force = false;
  unsigned int rate = 3;
  unsigned int thr  = 10;
  
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch(argv[i][1]) {
      case 'h': usage(argv[0]); return 0;
      case 'd': dns = argv[++i]; break;
      case 'o': out = argv[++i]; break;
      case 'n': name = argv[++i]; break;
      case 'p': pulse = true; break;
      case 'f': force = true; break;
      case 'r': str2val(argv[++i], rate); break;
      case 't': str2val(argv[++i], thr); break;
      default:
	std::cerr << argv[0] << ": Unknown option: '" << argv[i] << "'"
		  << std::endl;
	return 1;
      }
    }
  }
  MonitorService::fChangeThreshold = thr;
  
  std::ostream* pout = 0;
  if (!out.empty() && out != "-") 
    pout = new std::ofstream(out.c_str());
  else 
    pout = &std::cout;
  
  DimClient::setDnsNode(dns.c_str());

  Rcu rcu(name);
  
  bool first = true;
  
  while(true) {
    sleep(rate);
    if (pulse) std::cerr << "." << std::flush;
    
    if (!force && !rcu.IsChanged()) continue;
    if (pulse) std::cerr << std::endl;
    
    if (first) rcu.Print(*pout, Service::kHeader);
    first = false;
    rcu.Print(*pout, Service::kNatural);
    rcu.Processed();
  }
    
  return 0;
  
}
/*
 * EOF
 */
      
    

      

	
	
	 

  
