#!/bin/sh
prog=readStatus
det=0
dns="alifmddimdns"
srv=
log=

set -e 

recompile() 
{
    if test ${prog}.cxx -nt ${prog} ; then 
	g++ `rcuxx-config --ldflags --libs` ${prog}.cxx -o ${prog} 
    fi
}

usage()
{
    cat <<EOF
Wrapper script to call $prog with appropriate parameters. 

$prog is run with the standard output and error output redirected 
to a log file.  

The detector number should be passed with the -D option, as 

  $0 -D 1

for FMD1.  Additional options can be passed to $prog.  Below is the 
help message of $prog

EOF
    ./${prog} -h
}

recompile

while test $# -gt 0 ; do 
    case $1 in 
	-D)    det=$2; shift ;; 
	-h|--help) usage ; exit 0 ;; 
    esac
    shift
done 

if test $det -le 0 || test $det -gt 3 ; then 
    echo "No detector specified!" 
    exit 1
fi 

srv="FMD-FEE_0_0_${det}" 
log="Status_FMD${det}_"`date +%Y%m%d_%H%M%S`".log"

echo "Running ${prog} fee://${dns}/${srv} $@"
echo "Log file is $log"
nice ./${prog} fee://${dns}/${srv} $@ > $log 2>&1 &

#
# EOF
#
