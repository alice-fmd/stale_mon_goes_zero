//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUXX_FMD_H
# include <rcuxx/Fmd.h>
#endif
#ifndef RCUXX_FMD_FMDStatus_H
# include <rcuxx/fmd/FmdStatus.h>
#endif
#include <iostream>
#include <sstream>
#include <string>

struct Status : public Rcuxx::FmdStatus
{
  Status(Rcuxx::Rcu& rcu) : FmdStatus(rcu)
  {
  }
  unsigned int Value() const
  {
    return fData;
  }
};


void usage(const char* progname) 
{
  std::cout << "Usage: " << progname << " [OPTIONS] NODE\n\n" 
            << "\t-h\t\tThis help\n" 
            << "\t-e\t\tEmulate mode (no hardware)\n" 
            << "\t-d\t\tEnable debug output\n" 
            << std::endl;
}

template <typename T>
void
str2val(const char* str, T& v) 
{
  std::stringstream s(str);
  s >> v;
}

template <>
void
str2val<unsigned int>(const char* str, unsigned int& m) 
{
  std::stringstream s(str);
  if (str[0] == '0') {
    // s.get();
    std::cout << str << " is octal or hex" << std::endl;
    if (str[1] == 'x' || str[1] == 'X') {
      std::cout << str << " is hex" << std::endl;
      // s.get();
      s.setf(std::ios::hex, std::ios::basefield);
    }
    else {
      std::cout << str << " is octal" << std::endl;
      s.setf(std::ios::oct, std::ios::basefield);
    }
  }
  s >> m;
}

int main(int argc, char** argv)
{
  std::string  node("u2f:/dev/altro0");
  bool         emulate = false ,debug = false;
  int          n_iter = 3;
  unsigned int mask   = 0xffffffff;

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': usage(argv[0]); return 0;
      case 'e': emulate = true; break;
      case 'd': debug = true; break;
      case 'n': str2val(argv[++i], n_iter); break;
      case 'm': str2val(argv[++i], mask); break;
      }
    }
    else {
      node = argv[i];
    }
  }
  std::cout << "Mask is 0x" << std::hex << mask << std::dec << std::endl;
  int  tboards[] = { 0, 1, 16, 17, -1 };
  int  boards[]  = { -1, -1, -1, -1, -1 };
  int  j         = 0;
  int* ptboard   = tboards;
  while ((*ptboard) >= 0) {
    if (mask & (1 << *ptboard)) {
      boards[j] = *ptboard;
      j++;
    }
    ptboard++;
  }
  ptboard = boards;
  while ((*ptboard) >= 0) {
    std::cout << "Board " << *ptboard << " queried" << std::endl;
    ptboard++;
  }
      
  

  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(node.c_str(), emulate);  
  if (!rcu) return 1;
  if (debug) rcu->SetDebug(Rcuxx::Rcu::kRcu, 1);
  try {
    Rcuxx::Fmd fmd(*rcu);
    Status    status(*rcu);
    // Rcuxx::FmdStatus* status = fmd.Status();
    
    int* pboard = boards;
    while (*pboard >= 0) {
      status.SetAddress(*pboard);
	
      for (int iter = 0; iter < n_iter || n_iter < 0; iter++) { 
	unsigned int ret;
    
	if ((ret = status.Update())) {
          std::cerr << rcu->ErrorString(ret)
                    << " when reading from FEC # " << *pboard
                    << std::endl;
          continue;
        }
	
	unsigned int val = status.Value();	
	unsigned int mon = (val >> 0)  & 0x1F;
	unsigned int rx  = (val >> 5)  & 0x7;
	unsigned int tx  = (val >> 8)  & 0xF;
	unsigned int add = (val >> 12) & 0x7;
	
	std::cout << "FEC # " << *pboard 
		  << "\n  Monitor state:       ";
	switch(mon) {
	case 0x00:        std::cout << "idle";    break; 
	case 0x01:        std::cout << "start_1"; break;
	case 0x02:        std::cout << "start_2"; break;
	case 0x03:        std::cout << "latch_1"; break;
	case 0x04:        std::cout << "latch_2"; break;
	case 0x05:        std::cout << "latch_3"; break;
	case 0x06:        std::cout << "latch_4"; break;
	case 0x07:        std::cout << "ack_1";   break;
	case 0x08:        std::cout << "ack_2";   break;
	case 0x09:        std::cout << "ack_3";   break;
	case 0x0A:        std::cout << "stop_1";  break;
	case 0x0B:        std::cout << "stop_2";  break;
	case 0x0C:        std::cout << "stop_3";  break;
	case 0x0D:        std::cout << "latch_data_1";    break;
	case 0x0E:        std::cout << "latch_data_2";    break;
	case 0x0F:        std::cout << "latch_data_3";    break;
	case 0x10:        std::cout << "latch_data_4";    break;
	case 0x11:        std::cout << "ack_m_1"; break;
	case 0x12:        std::cout << "ack_m_2"; break;
	case 0x13:        std::cout << "ack_m_3"; break;
	case 0x14:        std::cout << "ack_m_4"; break;
	case 0x15:        std::cout << "n_ack_m_2";       break;
	case 0x16:        std::cout << "n_ack_m_3";       break;
	case 0x17:        std::cout << "n_ack_m_4";       break;
	case 0x18:        std::cout << "others";  break;
	default:          std::cout << "Unknown"; break;
	}

	std::cout << "\n  Reciever state:      ";
	switch (rx) {
	case 0:   std::cout << "idle";            break;
	case 1:   std::cout << "start";           break;
	case 2:   std::cout << "wait_dt";         break;
	case 3:   std::cout << "store_dt";        break;
	case 4:   std::cout << "ack_1";           break;
	case 5:   std::cout << "ack_2";           break;
	case 6:   std::cout << "wait_stop";       break;
	default:  std::cout << "Unknown";         break;
	}
      
	std::cout << "\n  Transmitter state:   ";
	switch (tx) {
	case 0:   std::cout << "idle";    break;
	case 1:   std::cout << "start";   break;
	case 2:   std::cout << "wait_add";        break;
	case 3:   std::cout << "store_add";       break;
	case 4:   std::cout << "ack_1";   break;
	case 5:   std::cout << "ack_2";   break;
	case 6:   std::cout << "load_1";  break;
	case 7:   std::cout << "load_2";  break;
	case 8:   std::cout << "wait_dt"; break;
	case 9:   std::cout << "store_dt";        break;
	case 10:  std::cout << "ack_master_1";    break;
	case 11:  std::cout << "ack_master_2";    break;
	case 12:  std::cout << "wait_stop_1";     break;
	case 13:  std::cout << "wait_stop_2";     break;
	case 14:  std::cout << "wait_stop_3";     break;
	case 15:  std::cout << "preload";         break;
	default:  std::cout << "Unknown";         break;
	}
      
	std::cout << "\n  Address decoder state: ";
	switch (add) {
	case 0:   std::cout << "idle";    break;
	case 1:   std::cout << "wait_start";      break;
	case 2:   std::cout << "start";   break;
	case 3:   std::cout << "wait_add";        break;
	case 4:   std::cout << "latch_add";       break;
	case 5:   std::cout << "fec_add"; break;
	case 6:   std::cout << "ack_1";   break;
	case 7:   std::cout << "s_rx";    break;
	case 8:   std::cout << "s_tx";    break;
	default:  std::cout << "Unknown";         break;
	}
	std::cout << std::endl;
	if (iter != n_iter-1 || n_iter < 0) sleep(1);
      } // for (iter)
      pboard++;
    } // while (pboard)
  }
  catch (unsigned int ret) {
    std::cerr << "Failed to update <" << ret 
	      << ">: " << rcu->ErrorString(ret) << std::endl;
    return 1;
  }
  return 0;
}
/*
 * EOF
 */

